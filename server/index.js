const port = process.env.PORT || 5000
const bodyParser = require('body-parser')
const express = require('express')
const app = express()
var cookieParser = require('cookie-parser')
const mongoose = require('mongoose')
const path = require('path')
const mongoURL = process.env.MONGO || 'mongodb://renofinsa:apaaja123@ds111244.mlab.com:11244/loginakses'
// const mongoURL = process.env.MONGO || 'mongodb://localhost:27017/RestAPI'
const SECRET = process.env.SECRET || 'asjd9x8cuz9xiuc01id9ixmozicn982klkczkc'
app.use(cookieParser(SECRET))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use((req,res,next) => {
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Allow-Method', 'GET, POST, PUT, PATCH, DELETE')
    res.setHeader('Access-Control-Allow-Header', 'Content-Type, Authorization')
    next()
  })
const api = express.Router()
api.use('/', require('./app/index'))

app.use('/document', express.static(path.join(__dirname, '../', '/')))

app.use('/api/v1', api)
mongoose.connect(mongoURL, {useNewUrlParser: true }
    ).then(result => {
        app.listen(port, () => console.log(`App listening on port ${port}!`))
    }).catch(err => console.log(err))
// mongoose connetection