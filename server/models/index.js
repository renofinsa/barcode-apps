const mongoose = require('mongoose')
const Schema = mongoose.Schema

const DataUndanganSchema = new Schema ({
  nama_undangan: {
    type: String,
    require: true
  },
  nomer_telepon: {
    type: String,
    require: true
  },
  code_undangan: {
    type: String,
    require: true
  }
},{timestamps: true})

module.exports = mongoose.model('dataUndangan', DataUndanganSchema)