const express = require('express')
const r = express.Router()
const Undangan = require('./../models/index')
const moment = require('moment')
const XlsxPopulate = require('xlsx-populate')

r.post('/', async (req,res) => {
    var {
        name,
        phone,
        code
    } = req.body
    var payload = {
        nama_undangan: name,
        nomer_telepon: phone,
        code_undangan: code
    }
    var check = await Undangan.findOne({code_undangan: code}).where({code_undangan: code})

    if(check){
        console.log(check)
        res.json({
            success : false,            
            message: 'data sudah tersedia'
        })
    }else{
        const data = new Undangan(payload)
        data.save().then(a => {
            res.json({
                success : true,
                message: 'success',
                results: a})
        }).catch(err => {
            console.log(err)
            res.json({
                success : false,
                message: 'failed'
            })
        })
    }
    
    

    // res.json({message: 'data tidak tersedia'})


    

    
})

r.get('/',(req,res) => {
    try {
        Undangan.find({}).sort({'createdAt': -1})
        .then(a => {
            var payload = []
            a.map((dt,index) => {
                payload.push({
                    no: index+1,
                    name: dt.nama_undangan,
                    phone: dt.nomer_telepon,
                    code: dt.code_undangan,
                    date: moment(dt.createdAt, "Asia/Makassar").format('h:mm, MMMM Do YYYY')
                })
            })
            if(payload == 0){
                res.json({
                    success : false,
                    message: 'Not Available '
                })
            }else{
                res.json({
                    success : true,                    
                    data: payload
                })
            }
        })
       
        

    } catch (error) {
        console.log(error)
        res.json({message: 'Error'})
    }
})

r.get('/random',(req,res) =>{
    try {
        Undangan.find({}).then(a => {
            var payload = []
            a.map((dt,index) => {
                payload.push({
                    no: index,
                    id: dt._id,
                    name: dt.nama_undangan,
                    phone: dt.nomer_telepon,
                    code: dt.code_undangan,
                })
            })

            var count = payload.length
            var value = Math.floor(Math.random() * count);
            console.log('nilai terpilih : '+value)
            res.json({
                success : false,                            
                data: payload[value]
            })
        })
    } catch (error) {
        console.log(error)
        res.json({
            success : false,            
            message: 'Error'
        })
    }
})

r.get('/count',(req,res) =>{
    try {
        Undangan.find({}).then(a => {
            var payload = []
            a.map((dt,index) => {
                payload.push({
                    no: index,
                    id: dt._id,
                    name: dt.nama_undangan,
                    phone: dt.nomer_telepon,
                    code: dt.code_undangan,
                })
            })

            var count = payload.length
            res.json({count: count})
        })
    } catch (error) {
        console.log(error)
        res.json({message: 'Error'})
    }
})

r.get('/download', async (req,res) => {
    var data = await Undangan.find({})
    var result = []
    data.map((b, index) => {
        result.push({
            no: index+1,
            name: b.nama_undangan,
            phone: b.nomer_telepon,
            code: b.code_undangan,
            date: moment(b.createdAt).format('h:mm a, MMMM Do YYYY') ,
        })
    })
  
console.log(data)
var count= Object.keys(result).length
  XlsxPopulate.fromBlankAsync()
    .then(workbook => {
      // Header
      workbook.sheet('Sheet1').cell('B1').value('Data Undangan').style('bold', true)
      // Header

      // Body
      // Table Head
      workbook.sheet('Sheet1').cell('A3').value('No')
      workbook.sheet('Sheet1').cell('B3').value('Nama')
      workbook.sheet('Sheet1').cell('C3').value('Nomer Induk Pegwai')
      workbook.sheet('Sheet1').cell('D3').value('Kode Undangan')
      workbook.sheet('Sheet1').cell('E3').value('Tanggal')
      // Table Head
      // Table Row
      const r = workbook.sheet('Sheet1').range(`A4:E${5+count}`)
      // r.value(5);
      r.value(
        result.map(a => 
          [a.no, a.name, a.phone, a.code,a.date],
        )
      )
      // Table Row
      // Body

      // Write to file.
      var dt = new Date()
      var dtformat = moment(dt).format('hhmmDDMMYYYY') 
      var nameFile = `Data_Undangan_${dtformat}.xlsx`
      workbook.toFileAsync(nameFile)
        .then(data => {
          console.log(nameFile)
          res.redirect(`/document/${nameFile}`)
        })
    })
})





module.exports = r