import React, { Component } from 'react'
import { Table,Button } from 'reactstrap';
import axios from 'axios'
import { Link } from 'react-router-dom';

class dataPages extends Component {
    constructor(){
        super()
        this.state = {
          data: []
        }
        this.onSubmit = this.onSubmit.bind(this)

      }
      componentDidMount() {
      axios
      .get('//indofest.ditamatech.com:8088/api/v1')
      .then(result => {
        console.log('tes', result)
        this.setState({
            data: result.data,
            loading: false
        })
      })
      .catch(error => {
        console.log(error.message, 'ini error')
        this.setState({
            error: error.message,
            loading: false
        })
      })
    }

    onSubmit(e){
        e.preventDefault()
        console.log('running')
        // with axios
        axios
          .get('//indofest.ditamatech.com:8088/api/v1/download')
          .then(res => {
            window.location.href = '/data'
          })
          .catch(err => {
              console.log('failed')
          })
           
      }


    render() {
        const { data } = this.state
        const ambilData = []
        data.map(a => {
            ambilData.push(
                <tr>
                    <td>{a.no}</td>
                    <td>{a.name}</td>
                    <td>{a.phone}</td>
                    <td>{a.code}</td>
                    <td>{a.date}</td>
                </tr>
            )
        })

        return (
            <div className="body-background">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6"><h1>Data Tamu Undangan</h1></div>
                        <div className="col-md-3">
                        {/* <form onSubmit={this.onSubmit}>
                        <input 
                type="submit"
                className="btn btn-primary form-control form-control-lg"
                value="Download"
              />
                        </form> */}
                            {/* <Button onSubmit={this.onSubmit} className="btn btn-success medep-kanan" >Download</Button> */}
                            <a href="//indofest.ditamatech.com:8088/api/v1/download" className="btn btn-success medep-kanan" >Download</a>
                        </div>
                        <div className="col-md-3">
                            <Link to="/" className="btn btn-danger medep-kanan" >Kembali</Link>
                        </div>
                    </div>
                    <Table className="table-1">
                <thead>
                    <th>#</th>
                    <th>Nama Undangan</th>
                    <th>Nomer Induk Pegawai</th>
                    <th>Kode Undangan</th>
                    <th>Tanggal</th>
                </thead>
                <tbody>
                    {ambilData}    
                    </tbody>
                </Table>                      
                </div>
            </div>
        )
    }
}

export default dataPages