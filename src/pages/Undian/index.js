import React, { Component } from 'react'
import './index.scss'
import { Card,NavLink, CardText, CardBody,
    CardTitle, CardSubtitle, Button } from 'reactstrap';
import axios from 'axios'
import { Link } from 'react-router-dom';


class Undian extends Component {
    constructor(){
        super()
        this.state = {
          nama : '',
          nomer_pegawai : '',
          code : '',
        }
      }

      componentDidMount() {
        axios
          .get('//indofest.ditamatech.com:8088/api/v1/random')
          .then(result => {
            console.log('tes', result)
            var a = result.data
            this.setState({
                nama : a.name,
                nomer_pegawai : a.phone,
                code : a.code,
                loading: false
            })
          })
          .catch(error => {
            console.log(error.message, 'ini error')
            this.setState({
              error: error.message,
              loading: false
            })
          })
        }


    render() {
        const { nama,nomer_pegawai,code } = this.state
        return (
            <div className="body-background align-center">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <Link to="/" className="btn btn-danger medep-kanan-index" >Kembali</Link>
                        </div>
                    </div>
                    <div>
                        <h1 className="text-align-center">WINNER</h1>
                        <Card className="card-size">
                            <CardBody>
                            <CardTitle className="color-black text-capitalize">{nama}</CardTitle>
                            <CardSubtitle className="color-black">{nomer_pegawai}</CardSubtitle>
                            <CardText className="color-black">{code}</CardText>
                            <NavLink href='/undian' className="btn btn-primary form-control">Memulai Undian</NavLink>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </div>
        )
    }
}
export default Undian