import React, { Component } from 'react'
import Button from './../components/button'
import { Link } from 'react-router-dom';
import Logo from './../img/logo.png'
import './../App.css'

class Index extends Component {

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={Logo} alt="logo" className="img-size" />
  
                    <div className="row">
                        <div className="col-md-6">
                            <Link className="btn btn-success button-size" to="/data">Data Tamu</Link>
                        </div>
                        <div className="col-md-6">
                            <Link className="btn btn-danger button-size" to="/undian">Mulai Undian</Link>
                        </div>
                    </div>
                </header>
            </div>
        )
    }
}
export default Index