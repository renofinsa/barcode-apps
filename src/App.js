import React, { Component } from 'react'


import { BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.scss'
import Index from './pages/index'
import NoMatch from './pages/404'
import DataPages from './pages/dataPages'
import Undian from './pages/Undian'

class App extends Component {
  render() {
    return (
      <Router>
        <div >
            <Route exact path="/" component={ Index } />
              <Route path="/data" component={ DataPages } />
              <Route path="/undian" component={ Undian } />
              <Route path="^" component={NoMatch} />
        </div>
      </Router>
    )
  }
}

export default App;
