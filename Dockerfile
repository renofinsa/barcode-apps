FROM node:alpine

# Create app directory
WORKDIR /usr/src/app
# VOLUME ["/public/"]
# ADD . /path/inside/docker/container
COPY package*.json ./

RUN apk add --no-cache --virtual .gyp \
        python \
        make \
        g++ \
    && npm install -g pm2 \
    && npm install -g babel-cli \
    && npm install \
    && apk del .gyp

COPY . .

EXPOSE 8088
CMD [ "pm2-runtime", "--env", "production", "ecosystem.config.json" ]